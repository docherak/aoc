# AOC 2023 - 01
[link](https://adventofcode.com/2023/day/1)

expected results:
- [x] part1: 55538
- [x] part2: 54875
