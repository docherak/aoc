use std::io;

fn main() {

    let mut numbers: Vec<u32> = vec![];
    let numbered_words = [("one", 1), ("two", 2), ("three", 3), ("four", 4), ("five", 5), ("six", 6), ("seven", 7), ("eight", 8), ("nine", 9)];

    loop {
        let mut line = String::new();
        let _ = io::stdin().read_line(&mut line);

        if line.trim().len() == 0 {
            break;
        }

        let mut digits: Vec<(usize, u32)> = line.chars().enumerate().filter(|c| c.1.is_numeric()).map(|c| (c.0, c.1.to_digit(10).unwrap())).collect(); 
        
        // part 2 block
        for &(word, digit) in &numbered_words {
            let mut start = 0;
            let word_length = word.len();
            while let Some(idx) = line[start..].find(word) {
                digits.push((start + idx, digit));
                start += idx + word_length;
            }
        }
        //

        digits.sort_by(|a, b| a.0.cmp(&b.0));
        let digits_len = digits.len();

        let number: u32 = match digits_len {
            1 => digits[0].1 * 11,
            _ => 10 * digits[0].1 + digits[digits_len - 1].1
        };
        numbers.push(number);

    }

    let result: u32 = numbers.into_iter().sum();
    println!("{}", result);

}
