use std::{env, fs, vec};

#[derive(Debug)]
struct Winner(u32);

impl Winner {
    fn win_or_lose(winning_ticket: &Vec<u32>, bet: u32) -> Option<Self> {
        if winning_ticket.iter().any(|&x| x == bet) {
            Some(Winner(bet))
        } else {
            None
        }
    }
}

#[derive(Debug)]
struct Ticket {
    winning_count: usize,
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut total_points: u32 = 0;
    let mut total_winners: u32 = 0;
    let mut tickets: Vec<Ticket> = vec![];

    let file_path = &args[1];

    let contents = fs::read_to_string(file_path)
        .expect("Should have been able to read the file");

    for line in contents.lines() {
        //let separator: usize = line.find("|").unwrap();
        
        let start_winning: usize = line.find(":").unwrap() + 2;
        let end_winning: usize = line.find("|").unwrap() - 1;
        let winning_numbers: Vec<u32> = line[start_winning..end_winning].split(" ").filter_map(|x| x.parse::<u32>().ok() ).collect();

        let start_bet: usize = line.find("|").unwrap() + 2;
        let end_bet: usize = line.len();
        let bet_numbers: Vec<u32> = line[start_bet..end_bet].split(" ").filter_map(|x| x.parse::<u32>().ok() ).collect();

        let matches: Vec<Option<Winner>> = bet_numbers.iter()
            .map(|&x| Winner::win_or_lose(&winning_numbers, x))
            .filter(|x| x.is_some())
            .collect();

        let mut points = 0;
        if !matches.is_empty() {
            points = (2 as u32).pow((matches.len() - 1) as u32);
        }

        tickets.push(Ticket {winning_count: matches.len()});

        total_points += points;
    
    }

    for (idx, _line) in contents.lines().enumerate() {
        let total_sum = sum_winning_tickets(&tickets, idx, true);
        total_winners += total_sum as u32;
    }

    println!("part1: {:?}", total_points);
    println!("part2: {:?}", total_winners);
}

fn sum_winning_tickets(tickets: &[Ticket], index: usize, original: bool) -> usize {
    if index >= tickets.len() { return 0 };

    let current_ticket = &tickets[index];
    let copies = current_ticket.winning_count;

    let mut sum = if original && copies > 0 { 1 } else {0};

    if original && copies == 0 {
        return 1;
    }
    if copies > 0 {
        sum += copies;
        for i in 1..=copies {
            sum += sum_winning_tickets(tickets, index + i, false);
        }
    }

    sum
}