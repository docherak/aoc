# AOC 2023 - 04
[link](https://adventofcode.com/2023/day/4)

expected results:
- [x] part1: 15268
- [x] part2: 6283755
