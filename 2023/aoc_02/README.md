# AOC 2023 - 02
[link](https://adventofcode.com/2023/day/2)

expected results:
- [x] part1: 2285
- [x] part2: 77021
