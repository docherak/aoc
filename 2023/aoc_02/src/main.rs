use core::panic;
use std::io;
use std::collections::HashMap;

const MAX_RED: u32 = 12;
const MAX_GREEN: u32 = 13;
const MAX_BLUE: u32 = 14;

enum Color {
    Red,
    Green,
    Blue
}

impl Color {
    fn color_from_string(color: &str) -> Option<Self> {
        match color {
            "red" => Some(Color::Red),
            "green" => Some(Color::Green),
            "blue" => Some(Color::Blue),
            _ => panic!("Invalid color: {}", color)
        }
    }
}

fn main() {

    let mut game_number: u32 = 0;
    let mut sum: u32 = 0;
    let mut sum_of_powers: u32 = 0;

    // 'lines: loop {
    loop {
        let mut line = String::new();
        let _ = io::stdin().read_line(&mut line);

        if line.trim().len() == 0 {
            break;
        }

        let mut game_possible = true;
        game_number += 1;

        let start_idx: usize = line.find(":").unwrap() + 2;
        let line_length = line.len();

        let separate: Vec<&str> = line[start_idx..line_length-1].split(";").map(|str| str.trim()).collect();
        let sections: Vec<Vec<&str>> = separate.iter().map(|row| row.split(",").map(|str| str.trim()).collect()).collect();

        let sections_of_pairs: Vec<HashMap<String, u32>> = sections.iter()
            .map(|vec| vec.iter()
                .map(|pair| {
                    let mut parts = pair.splitn(2, " ");
                    let number: u32 = parts.next().unwrap().parse::<u32>().unwrap();
                    let color = parts.next().unwrap().to_string();

                    (color, number)
                })
                .collect::<HashMap<String, u32>>()
            )
            .collect();

        let mut fewest_rgb: (u32, u32, u32) = (0, 0, 0);
        let mut rgb: (u32, u32, u32) = (0, 0, 0);
        for pairs in &sections_of_pairs {
            for (key, value) in pairs {
                match Color::color_from_string(key) {
                    Some(Color::Red) => { 
                        rgb.0 = *value;
                        if fewest_rgb.0 < *value { fewest_rgb.0 = *value; }
                    },
                    Some(Color::Green) => {
                        rgb.1 = *value;
                        if fewest_rgb.1 < *value { fewest_rgb.1 = *value; }
                    },
                    Some(Color::Blue) => {
                        rgb.2 = *value;
                        if fewest_rgb.2 < *value { fewest_rgb.2 = *value; }
                    },
                    None => println!("error")
                }
                if rgb.0 > MAX_RED || rgb.1 > MAX_GREEN || rgb.2 > MAX_BLUE {
                    // continue 'lines;
                    game_possible = false;
                }
            }
        }

        let power: u32 = fewest_rgb.0 * fewest_rgb.1 * fewest_rgb.2;

        if game_possible {
            sum += game_number;
        }

        sum_of_powers += power;

    }

    println!("{}", sum);
    println!("{}", sum_of_powers);
}
