# AOC 2023 - 06
[link](https://adventofcode.com/2023/day/6)

expected results:
- [x] part1: 2065338
- [x] part2: 34934171
