use std::io::{self, BufRead};

fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines();

    let mut times: Vec<u32> = vec![];
    let mut distances: Vec<u32> = vec![];

    if let Some(Ok(times_line)) = lines.next() {
        times = times_line.split_whitespace()
            .filter_map(|s| s.parse::<u32>().ok())
            .collect();
    }
    
    if let Some(Ok(distances_line)) = lines.next() {
        distances = distances_line.split_whitespace()
            .filter_map(|s| s.parse::<u32>().ok())
            .collect();
    }

    // Part 1
    let mut product: u32 = 1;
    for i in 0..times.len() {
        let count: usize = (0..=times[i]).map(|num| num*(times[i]-num)).filter(|num| num > &distances[i]).count();
        product *= count as u32;
    }
    println!("part1: {}", product);

    // Part 2
    let concat_time: u64 = times.iter().fold(0, |acc, &num| acc * 10u64.pow(if num == 0 { 1 } else { (num as f64).log10().trunc() as u32 + 1 }) + num as u64);
    let concat_distance: u64 = distances.iter().fold(0, |acc, &num| acc * 10u64.pow(if num == 0 { 1 } else { (num as f64).log10().trunc() as u32 + 1 }) + num as u64);

    let count: usize = (0..=concat_time).map(|num| num*(concat_time-num)).filter(|num| num > &concat_distance).count();
    println!("part2: {}", count);

}
