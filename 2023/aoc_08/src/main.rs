use std::collections::HashMap;

enum Direction {
    Left,
    Right
}

impl Direction {
    fn new(direction_input: char) -> Option<Direction> {
        match direction_input {
            'L' => Some(Direction::Left),
            'R' => Some(Direction::Right),
            _ => None
        }
    }
}

struct Step {
    left: u32,
    right: u32 
}

impl Step {
    fn new(left: u32, right: u32) -> Step {
        Step {
            left,
            right
        }
    }
}

const INPUT_FILE: &str = include_str!("../input.txt");

fn main() {

    let mut hashmap: HashMap<u32, Step> = HashMap::new();
    let mut directions: Vec<Direction> = vec![];


    let mut lines = INPUT_FILE.lines();
    if let Some(line) = lines.next() {
        directions = line.chars().filter_map(|char| Direction::new(char)).collect();
    }

    let mut current_step: u32 = 0;

    for line in lines {
        if let Some((name, left, right)) = parse_step(line) {
            if name == 656565 {
                current_step = 656565;
                hashmap.insert(name, Step::new(left, right));
                continue;
            }
            hashmap.insert(name, Step::new(left, right));
        } 
    }

    let mut steps = 0;
    for dir in directions.iter().cycle() {
        if current_step == 909090 {
            break;
        }
        steps += 1;
        current_step = match dir {
            Direction::Left => hashmap.get(&current_step).unwrap().left,
            Direction::Right => hashmap.get(&current_step).unwrap().right
        };
    }

    println!("part1: {:?}", steps);
}

fn parse_step(line: &str) -> Option<(u32, u32, u32)> {
    if line.is_empty() {
        return None;
    }
    let numbers: Vec<u32> = line.as_bytes()
        .iter()
        .filter_map(|num| {
            let number = match num {
                32 | 61 | 40 | 44 | 41 => None,
                _ => Some(*num as u32)
            };
            number
        })
        .collect();

    let struct_info: Vec<u32> = numbers.chunks(3)
        .map(|chunk| chunk[0] * 10_u32.pow(4) + chunk[1] * 10_u32.pow(2) + chunk[2])
        .collect();

    Some((struct_info[0], struct_info[1], struct_info[2]))
}

