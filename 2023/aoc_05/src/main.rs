use std::{io, collections::HashMap};

#[derive(Debug, Clone, Copy)]
struct Map(u64, u64);

fn main() {

    let mut seeds: Vec<u64> = vec![];
    let mut first_line: bool = true;
    let mut seed_tracker: HashMap<u64, (Map, bool)> = HashMap::new();

    loop {
        let mut line = String::new();
        let _ = io::stdin().read_line(&mut line);

        if line.as_bytes().is_empty() { 
            break;
        }
        
        if line.is_empty() {
            continue;
        }
        
        if first_line {
            seeds = line.split_whitespace()
                .skip(1)
                .filter_map(|number| number.parse::<u64>().ok())
                .collect();

            first_line = false;

        } else if line.chars().next().unwrap().is_alphabetic() {
            seeds.iter().for_each(|seed| reset_bool(*seed, &mut seed_tracker));

        } else if line.chars().next().unwrap().is_numeric(){
            let map: Vec<u64> = line.split_whitespace()
                .filter_map(|number| number.parse::<u64>().ok())
                .take(3)
                .collect();

            let unchecked_seeds: Vec<&u64> = seeds.iter().filter(|seed| !seed_tracker.get(seed).unwrap().1).collect();
            unchecked_seeds.iter().for_each(|seed| update_seed(**seed, &map, &mut seed_tracker));
        }
    }

    let min_location: Option<(Map, bool)> = seed_tracker.into_values().min_by_key(|x| x.0.1);
    println!("part1: {:?}", min_location.unwrap().0.1);
}

// seed tracker
fn update_seed(seed: u64, map: &Vec<u64>, seed_tracker: &mut HashMap<u64, (Map, bool)>) {
    let current_map = seed_tracker.get(&seed).unwrap().0;

    if current_map.1 >= map[1] && current_map.1 <= map[1] + map[2] {
        let diff = current_map.1 - map[1];
        seed_tracker.insert(seed, (Map(current_map.1, map[0] + diff), true));
    }
}

fn reset_bool(seed: u64, seed_tracker: &mut HashMap<u64, (Map, bool)>) {
    match seed_tracker.get(&seed) {
        Some((source, true)) => seed_tracker.insert(seed, (*source, false)),
        None => seed_tracker.insert(seed, (Map(seed, seed), false)),
        Some((source, _)) => seed_tracker.insert(seed, (*source, false))
    };
}