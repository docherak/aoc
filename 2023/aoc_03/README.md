# AOC 2023 - 03
[link](https://adventofcode.com/2023/day/3)

expected results:
- [x] part1: 532428
- [x] part2: 84051670
