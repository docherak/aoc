use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut grid: Vec<&str> = vec![];
    let mut sum: u32 = 0;
    let mut sum_of_products: u32 = 0;

    let file_path = &args[1];

    let contents = fs::read_to_string(file_path)
        .expect("Should have been able to read the file");

    for line in contents.lines() {
        grid.push(line);
    }

    let grid_length: i32 = grid.len() as i32;
    let row_length: i32 = grid[0].len() as i32;

    for (i, row) in grid.iter().enumerate() {
        let row: Vec<char> = row.chars().collect();
        let mut first_occurence = 0;
        for (j, &ch) in row.iter().enumerate() {
            if ch == '*' {
                if first_occurence > 0 {
                    first_occurence -= 1;
                }
                let mut gears: Vec<u32> = vec![];
                let mut last_start: usize = 0;
                let mut last_end: usize = 0;
                let mut last_row: i32 = 0;
                for x in -1..=1 {
                    for y in -1..=1 {
                        if x == 0 && y == 0 { continue; }

                        let adj_i = i as i32 + x;
                        let adj_j = j as i32 + y;

                        if adj_i >= 0 && adj_i < grid_length && adj_j >= 0 && adj_j < row_length {
                            let adjacent_cell = grid[adj_i as usize].chars().nth(adj_j as usize).unwrap();
                            if adjacent_cell.is_digit(10) {
                                let adj_row: Vec<char> = grid[adj_i as usize].chars().collect();
                                let num = get_number(&adj_row, adj_j as usize);
                                
                                if last_row == adj_i && number_start_idx(&adj_row, adj_j as usize) == last_start && number_end_idx(&adj_row, adj_j as usize) == last_end {
                                    continue;
                                }

                                last_start = number_start_idx(&adj_row, adj_j as usize);
                                last_end = number_end_idx(&adj_row, adj_j as usize);
                                last_row = adj_i;
                                gears.push(num.unwrap());
                            }
                        }
                    }
                }
                if gears.len() == 2 {
                    let product: u32 = gears.iter().product();
                    sum_of_products += product;
                }
            } else {
                if first_occurence > 0 {
                    first_occurence -= 1;
                    continue;
                }
                if ch.is_digit(10) {
                    for x in -1..=1 {
                        for y in -1..=1 {
                            if x == 0 && y == 0 { continue; }
                            let adj_i = i as i32 + x;
                            let adj_j = j as i32 + y;

                            if adj_i >= 0 && adj_i < grid_length && adj_j >= 0 && adj_j < row_length {
                                let adjacent_cell = grid[adj_i as usize].chars().nth(adj_j as usize).unwrap();
                                if !adjacent_cell.is_digit(10) && adjacent_cell != '.' {
                                    let num = get_number(&row, j);
                                    sum += num.unwrap();
                                    let end = number_end_idx(&row, j);
                                    first_occurence = end as u32 - j as u32;
                                }
                            }

                        }
                    }
                }

            }
        }
    }
    println!("part1:{}", sum);
    println!("part2:{}", sum_of_products);
}

fn get_number(row: &Vec<char>, index: usize) -> Option<u32> {
    let start = number_start_idx(row, index);
    let end = number_end_idx(row, index);

    if start == 0 && end == 0 {
        return row[start].to_digit(10);
    }
    let number: String = row[start..end].iter().collect();
    number.parse::<u32>().ok()
}

fn number_start_idx(row: &Vec<char>, index: usize) -> usize {
    let mut start = index;
    while start > 0 && row[start - 1].is_digit(10) {
        start -= 1;
    }
    start
}

fn number_end_idx(row: &Vec<char>, index: usize) -> usize {
    let mut end = index + 1;
    while end < row.len() && row[end].is_digit(10) {
        end += 1;
    }
    end
}