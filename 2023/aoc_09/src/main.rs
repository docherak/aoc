use std::io::{self, BufRead};

fn main() {
    let stdin = io::stdin();
    let lines = stdin.lock().lines();

    let mut top_right_total: i64 = 0;
    let mut top_left_total: i64 = 0;
    
    for line in lines.filter_map(|line| line.ok()) {
        let sequence: Vec<i64> = line.split_whitespace()
            .filter_map(|number| number.parse::<i64>().ok())
            .collect();

        top_right_total += top_right(&sequence, 0);
        top_left_total += top_left(&sequence, 0, 0);
    }
    println!("part1: {}", top_right_total);
    println!("part2: {}", top_left_total);
}

fn top_right(sequence: &Vec<i64>, mut sum: i64) -> i64 {
    if sequence.iter().all(|&x| x == 0) {
        return sum;
    }

    if let Some(&last) = sequence.last() {
        sum += last;
    }

    let next_sequence: Vec<i64> = sequence.windows(2)
        .map(|window| window[1] - window[0])
        .collect();

    top_right(&next_sequence, sum)
}

fn top_left(sequence: &Vec<i64>, mut sum: i64, line: usize) -> i64 {
    if sequence.iter().all(|&x| x == 0) {
        return sum;
    }

    if let Some(&first) = sequence.first() {
        if line % 2 == 0 {
            sum += first;
        } else if line % 2 == 1 {
            sum -= first;
        }
    }

    let next_sequence: Vec<i64> = sequence.windows(2)
        .map(|window| window[1] - window[0])
        .collect();

    top_left(&next_sequence, sum, line + 1)
}