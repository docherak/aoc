# AOC 2023 - 09
[link](https://adventofcode.com/2023/day/9)

expected results:
- [x] part1: 1904165718
- [x] part2: 964
